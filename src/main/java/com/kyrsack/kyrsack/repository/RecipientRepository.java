package com.kyrsack.kyrsack.repository;

import com.kyrsack.kyrsack.model.User;
import com.kyrsack.kyrsack.recipient.Recipient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RecipientRepository extends JpaRepository<Recipient,Long> {
    List<Recipient> findByRecipientSubject(String recipientSubject);


}
