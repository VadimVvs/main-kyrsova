package com.kyrsack.kyrsack.repository;

import com.kyrsack.kyrsack.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
    User findByUsername(String username);
    User findByActivationCode (String code);


}
