package com.kyrsack.kyrsack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class KyrsackApplication {

	public static void main(String[] args) {
		SpringApplication.run(KyrsackApplication.class, args);
	}

}
