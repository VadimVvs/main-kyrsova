package com.kyrsack.kyrsack.recipient;

import com.kyrsack.kyrsack.model.User;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Recipient {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long recipientId;

    private String recipientEmail;
    private String recipientSubject;
    private String recipientText;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User author;

    public Recipient (){}

    public Recipient(String recipientEmail, String recipientSubject, String recipientText) {
        this.recipientEmail = recipientEmail;
        this.recipientSubject = recipientSubject;
        this.recipientText = recipientText;
    }

    public Long getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(Long recipientId) {
        this.recipientId = recipientId;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public String getRecipientSubject() {
        return recipientSubject;
    }

    public void setRecipientSubject(String recipientSubject) {
        this.recipientSubject = recipientSubject;
    }

    public String getRecipientText() {
        return recipientText;
    }

    public void setRecipientText(String recipientText) {
        this.recipientText = recipientText;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }
    public String getAuthorName() {
        return author != null ? author.getUsername() : "<none>";
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Recipient recipient = (Recipient) o;
        return Objects.equals(recipientId, recipient.recipientId) &&
                Objects.equals(recipientEmail, recipient.recipientEmail) &&
                Objects.equals(recipientSubject, recipient.recipientSubject) &&
                Objects.equals(recipientText, recipient.recipientText) &&
                Objects.equals(author, recipient.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recipientId, recipientEmail, recipientSubject, recipientText, author);
    }

    @Override
    public String toString() {
        return "Recipient{" +
                "recipientId=" + recipientId +
                ", recipientEmail='" + recipientEmail + '\'' +
                ", recipientSubject='" + recipientSubject + '\'' +
                ", recipientText='" + recipientText + '\'' +
                ", author=" + author +
                '}';
    }
}
