package com.kyrsack.kyrsack.controllers;


import com.kyrsack.kyrsack.mail.MailService;
import com.kyrsack.kyrsack.model.User;
import com.kyrsack.kyrsack.recipient.Recipient;


import com.kyrsack.kyrsack.repository.RecipientRepository;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;



@Controller
public class MainController {
    @Autowired
    private MailService smtpMailSender;
    @Autowired
    private RecipientRepository recipientRepository;


    @GetMapping("/")
    public String mailView(Model model) {
        return "index";

    }

    @RequestMapping(value = "/show", method = RequestMethod.POST)
    public String showHtml(Model model, @RequestParam(value = "html", defaultValue = "") String html,
                           @RequestParam(value = "jsone", defaultValue = "") String jsone) {

        model.addAttribute("htmls", html);
        return "index";
    }


    @GetMapping("/history")
    public String history(Model model, @AuthenticationPrincipal User user) {
        List<Recipient> list = smtpMailSender.getAll();

        model.addAttribute("messages", list);
        return "history";
    }


    @RequestMapping(value = "/send", method = RequestMethod.POST)
    public String send(Model model, @RequestParam(value = "html", defaultValue = " ") String html,
                       @RequestParam String jsone,
                       @AuthenticationPrincipal User user) {

        Recipient recipient = new Recipient();
        JSONArray obj = new JSONArray(jsone);
        for (int i = 0; i < obj.length(); i++) {
            recipient.setAuthor(user);
            recipient.setRecipientEmail(obj.getJSONObject(i).getString("recipientEmail"));
            recipient.setRecipientSubject(obj.getJSONObject(i).getString("recipientSubject"));
            recipient.setRecipientText(html);
            smtpMailSender.save(recipient);
            smtpMailSender.sendEmail(recipient);

        }

        //  smtpMailSender.sendEmail(recipient);
//        System.out.println(jsone);
        return "redirect:/";
    }
}
