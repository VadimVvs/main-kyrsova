package com.kyrsack.kyrsack.controllers;

import com.kyrsack.kyrsack.mail.MailService;
import com.kyrsack.kyrsack.model.User;
import com.kyrsack.kyrsack.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Controller
public class UserHomeController {

    @Autowired
    private UserService userService;

    @Autowired
    private MailService mailService;

    @GetMapping("/home")
    public String showHomePage(Model model, @AuthenticationPrincipal User user) {
        model.addAttribute("usern", user.getUsername());
        model.addAttribute("emails", user.getEmail());
        model.addAttribute("roless", user.getRoles());

        return "home";
    }

    @RequestMapping(value = "/set", method = RequestMethod.POST)
    public String setNewBean(Model model, @RequestParam @NotBlank(message = "email is empty") @Email String email
            , @NotBlank(message = "password is empty") @RequestParam("password") String password
            , @RequestParam(defaultValue = " ") String message) {
        if (!StringUtils.isEmpty(email) && !email.isEmpty()) {
            message = "your email has been chanched";
            model.addAttribute("message", message);
            mailService.setNewBean(email, password);

        } else {
            message = "some errors";
            model.addAttribute("message", message);

        }

        return "home";
    }

    @RequestMapping(value = "/default", method = RequestMethod.POST)
    public String setDefaultBean(Model model) {

        model.addAttribute("message", "your email has been chanched in Default");
        mailService.setDefaultBean();
        return "home";
    }


}
