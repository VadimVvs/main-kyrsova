package com.kyrsack.kyrsack.mail;

import com.kyrsack.kyrsack.recipient.Recipient;
import com.kyrsack.kyrsack.repository.RecipientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Service
public class MailService {
    @Autowired
    private JavaMailSender mailSender;


//    @Autowired
//    private Configuration configuration; for templates

    @Value("${spring.mail.username}")
    private String username;

    @Value("${spring.mail.password}")
    private String password;

    @Autowired
    private RecipientRepository recipientRepository;

    public Recipient save(Recipient recipient) {
        return recipientRepository.save(recipient);

    }

    public List<Recipient> getAll() {
        return recipientRepository.findAll();

    }

    public MailResponse sendEmail(Recipient request) {
        MailResponse response = new MailResponse();
        MimeMessage message = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());


            helper.setTo(request.getRecipientEmail());
            helper.setText(request.getRecipientText(), true);
            helper.setSubject(request.getRecipientSubject());
            helper.setFrom(new InternetAddress("lolipop57300@gmail.com", "MailSender"));
            mailSender.send(message);
            response.setMessage("mail send to : " + request.getRecipientEmail());
            response.setStatus(Boolean.TRUE);

        } catch (MessagingException | UnsupportedEncodingException e) {
            response.setMessage("Mail Sending failure : " + e.getMessage());
            response.setStatus(Boolean.FALSE);

        }
        return response;
    }

    public void setNewBean(String UserMail, String UserPassword) {

        ((JavaMailSenderImpl) mailSender).setUsername(UserMail);
        ((JavaMailSenderImpl) mailSender).setPassword(UserPassword);
    }

    public void setDefaultBean() {
        ((JavaMailSenderImpl) mailSender).setUsername(username);
        ((JavaMailSenderImpl) mailSender).setPassword(password);
    }

}
